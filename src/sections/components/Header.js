import React from 'react';
import { View, Text, Image, SafeAreaView, StyleSheet } from 'react-native';

function Header(props) {
  return(
    <View>
        <SafeAreaView>

          <View style={ styles.container }>
            <Image
              source = { require('../../../assets/The1984.png') }
              style = { styles.logo }
            />  
          
            <View style={ styles.texto } >
              { props.children }
            </View>

          </View>          
          
        </SafeAreaView>
    </View>  
  )
}

const styles = StyleSheet.create({
  container : { 
    paddingVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row'
  },
  texto: {
    flex: 1, 
    flexDirection: 'row',
    justifyContent: 'flex-end' 
  },
  logo: { 
    width: 25, 
    height: 25,
    resizeMode: 'contain'
  }
})

export default Header;